#!/bin/bash
# script to build vpx,vgx,vlx,vpv,gse,whe and pse emissions using the NSW EPA 2012 inventory
# mec csiro 18/7/2015
# Modifications
#-------------------------------------------------------------------------------------------
# who   when    what
# kjm 28/08/19  update with new OS, pm25, 2013 PM2.5/PM10 ratios
# mec 16/03/16  set emission year
# mec 24/12/15  shipping speciation now for dry mass
#               mv SO4 scaled from PM10
# mec  3/12/15  added user input vbs and SO3oPM
#             SO3oPM < 0 - use SO3oSOx else use SO3oPM
# mec  1/12/15  other is now 70% coarse fraction OT
#
# set up scenarios and options
# 
export yyyy=$1
export mm=$2
export mm_name=$3
export dd=$4
export offset=$5
#export datapath=$6
export de=$6 #$7

# testing
#export yyyy=2010
#export mm=07
#export mm_name=jul08
#export dd=01
#export de=07
#export offset=-10
#export datapath=/mnt/climate/cas/project/Emission_2013_CIT
export datapath=../../../../emission2013NH3/PM2.5
export exepath=/mnt/climate/cas/project/NRDiesel/models/buildemission/pm25

# Emissions year
export emyr=13
export mm_name_mm=$mm_name${emyr} 
# These emissions are modified for temperature in the CTM and
# thus are fixed to specific months here to avoid double counting
export mm_name_veh=feb${emyr}
export offset_veh=-10
export mm_name_whe=jul${emyr}
export offset_whe=-10

#testing
#goto whe
#goto gse 
#goto vlx
#goto pse
#goto psemerge

#----------------------------------------------------------------------
#vpx:
#----------------------------------------------------------------------
cat <<eof >petExh.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Weekday file name
${datapath}/VPX/mvems_${mm_name_veh}wkday_petrolexhaust_NH3.in 
Weekend file name
${datapath}/VPX/mvems_${mm_name_veh}wkend_petrolexhaust_NH3.in 
Output TAPM file generic name
emsns_gmr.vpx.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset_veh
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.953,0.283,0.635,0.083,0.0,0.044,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.185,0.340,0.085
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP?
F
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
eof

${exepath}/build_motorVehicle_emissions_glo/build_motorvehicle_emissions_glo petExh.run >&petExh_${mm_name}.trace
cp wkday_emissions_kgPerHour.csv wkday_emissions_vpx_geo_kgPerHour_cb05.csv
cp wkend_emissions_kgPerHour.csv wkend_emissions_vpx_geo_kgPerHour_cb05.csv

cp CITemissions_wkday_NO_kgPerDay.grd CITemissions_wkday_vpx_NO_kgPerDay_cb05.grd
cp CITemissions_wkend_NO_kgPerDay.grd CITemissions_wkend_vpx_NO_kgPerDay_cb05.grd
cp CITemissions_wkday_NO2_kgPerDay.grd CITemissions_wkday_vpx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkend_NO2_kgPerDay.grd CITemissions_wkend_vpx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkday_CO_kgPerDay.grd CITemissions_wkday_vpx_CO_kgPerDay_cb05.grd
cp CITemissions_wkend_CO_kgPerDay.grd CITemissions_wkend_vpx_CO_kgPerDay_cb05.grd
cp CITemissions_wkday_SO2_kgPerDay.grd CITemissions_wkday_vpx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkend_SO2_kgPerDay.grd CITemissions_wkend_vpx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkday_ALD2_kgPerDay.grd CITemissions_wkday_vpx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkend_ALD2_kgPerDay.grd CITemissions_wkend_vpx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkday_PART_kgPerDay.grd CITemissions_wkday_vpx_PART_kgPerDay_cb05.grd
cp CITemissions_wkend_PART_kgPerDay.grd CITemissions_wkend_vpx_PART_kgPerDay_cb05.grd
cp CITemissions_wkday_ETH_kgPerDay.grd CITemissions_wkday_vpx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETH_kgPerDay.grd CITemissions_wkend_vpx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkday_FORM_kgPerDay.grd CITemissions_wkday_vpx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkend_FORM_kgPerDay.grd CITemissions_wkend_vpx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkday_ISOP_kgPerDay.grd CITemissions_wkday_vpx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkend_ISOP_kgPerDay.grd CITemissions_wkend_vpx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkday_OLE_kgPerDay.grd CITemissions_wkday_vpx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkend_OLE_kgPerDay.grd CITemissions_wkend_vpx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkday_PAR_kgPerDay.grd CITemissions_wkday_vpx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkend_PAR_kgPerDay.grd CITemissions_wkend_vpx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkday_TOL_kgPerDay.grd CITemissions_wkday_vpx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkend_TOL_kgPerDay.grd CITemissions_wkend_vpx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkday_XYL_kgPerDay.grd CITemissions_wkday_vpx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkend_XYL_kgPerDay.grd CITemissions_wkend_vpx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkday_ETOH_kgPerDay.grd CITemissions_wkday_vpx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETOH_kgPerDay.grd CITemissions_wkend_vpx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkday_MEOH_kgPerDay.grd CITemissions_wkday_vpx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkend_MEOH_kgPerDay.grd CITemissions_wkend_vpx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkday_UNR_kgPerDay.grd CITemissions_wkday_vpx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkend_UNR_kgPerDay.grd CITemissions_wkend_vpx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkday_NH3_kgPerDay.grd CITemissions_wkday_vpx_NH3_kgPerDay_cb05.grd
cp CITemissions_wkend_NH3_kgPerDay.grd CITemissions_wkend_vpx_NH3_kgPerDay_cb05.grd

#----------------------------------------------------------------------
#vdx:
#----------------------------------------------------------------------
cat <<eof >dieExh.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Weekday file name
${datapath}/VDX/mvems_${mm_name_veh}wkday_dieselexhaust_NH3.in 
Weekend file name
${datapath}/VDX/mvems_${mm_name_veh}wkend_dieselexhaust_NH3.in 
Output TAPM file generic name
emsns_gmr.vdx.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset_veh
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.970,0.329,0.401,0.270,0.0,0.0285,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.374,0.145,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP?
F
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
eof

${exepath}/build_motorVehicle_emissions_glo/build_motorvehicle_emissions_glo dieExh.run >&dieExh_${mm_name}.trace
cp wkday_emissions_kgPerHour.csv wkday_emissions_vdx_geo_kgPerHour_cb05.csv
cp wkend_emissions_kgPerHour.csv wkend_emissions_vdx_geo_kgPerHour_cb05.csv

cp CITemissions_wkday_NO_kgPerDay.grd CITemissions_wkday_vdx_NO_kgPerDay_cb05.grd
cp CITemissions_wkend_NO_kgPerDay.grd CITemissions_wkend_vdx_NO_kgPerDay_cb05.grd
cp CITemissions_wkday_NO2_kgPerDay.grd CITemissions_wkday_vdx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkend_NO2_kgPerDay.grd CITemissions_wkend_vdx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkday_CO_kgPerDay.grd CITemissions_wkday_vdx_CO_kgPerDay_cb05.grd
cp CITemissions_wkend_CO_kgPerDay.grd CITemissions_wkend_vdx_CO_kgPerDay_cb05.grd
cp CITemissions_wkday_SO2_kgPerDay.grd CITemissions_wkday_vdx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkend_SO2_kgPerDay.grd CITemissions_wkend_vdx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkday_ALD2_kgPerDay.grd CITemissions_wkday_vdx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkend_ALD2_kgPerDay.grd CITemissions_wkend_vdx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkday_PART_kgPerDay.grd CITemissions_wkday_vdx_PART_kgPerDay_cb05.grd
cp CITemissions_wkend_PART_kgPerDay.grd CITemissions_wkend_vdx_PART_kgPerDay_cb05.grd
cp CITemissions_wkday_ETH_kgPerDay.grd CITemissions_wkday_vdx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETH_kgPerDay.grd CITemissions_wkend_vdx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkday_FORM_kgPerDay.grd CITemissions_wkday_vdx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkend_FORM_kgPerDay.grd CITemissions_wkend_vdx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkday_ISOP_kgPerDay.grd CITemissions_wkday_vdx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkend_ISOP_kgPerDay.grd CITemissions_wkend_vdx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkday_OLE_kgPerDay.grd CITemissions_wkday_vdx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkend_OLE_kgPerDay.grd CITemissions_wkend_vdx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkday_PAR_kgPerDay.grd CITemissions_wkday_vdx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkend_PAR_kgPerDay.grd CITemissions_wkend_vdx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkday_TOL_kgPerDay.grd CITemissions_wkday_vdx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkend_TOL_kgPerDay.grd CITemissions_wkend_vdx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkday_XYL_kgPerDay.grd CITemissions_wkday_vdx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkend_XYL_kgPerDay.grd CITemissions_wkend_vdx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkday_ETOH_kgPerDay.grd CITemissions_wkday_vdx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETOH_kgPerDay.grd CITemissions_wkend_vdx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkday_MEOH_kgPerDay.grd CITemissions_wkday_vdx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkend_MEOH_kgPerDay.grd CITemissions_wkend_vdx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkday_UNR_kgPerDay.grd CITemissions_wkday_vdx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkend_UNR_kgPerDay.grd CITemissions_wkend_vdx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkday_NH3_kgPerDay.grd CITemissions_wkday_vdx_NH3_kgPerDay_cb05.grd
cp CITemissions_wkend_NH3_kgPerDay.grd CITemissions_wkend_vdx_NH3_kgPerDay_cb05.grd


#----------------------------------------------------------------------
#vlx:
#----------------------------------------------------------------------
cat <<eof >lpgExh.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Weekday file name
${datapath}/VLX/mvems_${mm_name_veh}wkday_otherexhaust_NH3.in 
Weekend file name
${datapath}/VLX/mvems_${mm_name_veh}wkend_otherexhaust_NH3.in 
Output TAPM file generic name
emsns_gmr.vlx.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset_veh
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.953,0.283,0.635,0.083,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.491,0.650,0.123
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP?
F
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
eof

${exepath}/build_motorVehicle_emissions_glo/build_motorvehicle_emissions_glo lpgExh.run >&lpgExh_${mm_name}.trace
cp wkday_emissions_kgPerHour.csv wkday_emissions_vlx_geo_kgPerHour_cb05.csv
cp wkend_emissions_kgPerHour.csv wkend_emissions_vlx_geo_kgPerHour_cb05.csv

cp CITemissions_wkday_NO_kgPerDay.grd CITemissions_wkday_vlx_NO_kgPerDay_cb05.grd
cp CITemissions_wkend_NO_kgPerDay.grd CITemissions_wkend_vlx_NO_kgPerDay_cb05.grd
cp CITemissions_wkday_NO2_kgPerDay.grd CITemissions_wkday_vlx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkend_NO2_kgPerDay.grd CITemissions_wkend_vlx_NO2_kgPerDay_cb05.grd
cp CITemissions_wkday_CO_kgPerDay.grd CITemissions_wkday_vlx_CO_kgPerDay_cb05.grd
cp CITemissions_wkend_CO_kgPerDay.grd CITemissions_wkend_vlx_CO_kgPerDay_cb05.grd
cp CITemissions_wkday_SO2_kgPerDay.grd CITemissions_wkday_vlx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkend_SO2_kgPerDay.grd CITemissions_wkend_vlx_SO2_kgPerDay_cb05.grd
cp CITemissions_wkday_ALD2_kgPerDay.grd CITemissions_wkday_vlx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkend_ALD2_kgPerDay.grd CITemissions_wkend_vlx_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkday_PART_kgPerDay.grd CITemissions_wkday_vlx_PART_kgPerDay_cb05.grd
cp CITemissions_wkend_PART_kgPerDay.grd CITemissions_wkend_vlx_PART_kgPerDay_cb05.grd
cp CITemissions_wkday_ETH_kgPerDay.grd CITemissions_wkday_vlx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETH_kgPerDay.grd CITemissions_wkend_vlx_ETH_kgPerDay_cb05.grd
cp CITemissions_wkday_FORM_kgPerDay.grd CITemissions_wkday_vlx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkend_FORM_kgPerDay.grd CITemissions_wkend_vlx_FORM_kgPerDay_cb05.grd
cp CITemissions_wkday_ISOP_kgPerDay.grd CITemissions_wkday_vlx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkend_ISOP_kgPerDay.grd CITemissions_wkend_vlx_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkday_OLE_kgPerDay.grd CITemissions_wkday_vlx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkend_OLE_kgPerDay.grd CITemissions_wkend_vlx_OLE_kgPerDay_cb05.grd
cp CITemissions_wkday_PAR_kgPerDay.grd CITemissions_wkday_vlx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkend_PAR_kgPerDay.grd CITemissions_wkend_vlx_PAR_kgPerDay_cb05.grd
cp CITemissions_wkday_TOL_kgPerDay.grd CITemissions_wkday_vlx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkend_TOL_kgPerDay.grd CITemissions_wkend_vlx_TOL_kgPerDay_cb05.grd
cp CITemissions_wkday_XYL_kgPerDay.grd CITemissions_wkday_vlx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkend_XYL_kgPerDay.grd CITemissions_wkend_vlx_XYL_kgPerDay_cb05.grd
cp CITemissions_wkday_ETOH_kgPerDay.grd CITemissions_wkday_vlx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETOH_kgPerDay.grd CITemissions_wkend_vlx_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkday_MEOH_kgPerDay.grd CITemissions_wkday_vlx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkend_MEOH_kgPerDay.grd CITemissions_wkend_vlx_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkday_UNR_kgPerDay.grd CITemissions_wkday_vlx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkend_UNR_kgPerDay.grd CITemissions_wkend_vlx_UNR_kgPerDay_cb05.grd
cp CITemissions_wkday_NH3_kgPerDay.grd CITemissions_wkday_vlx_NH3_kgPerDay_cb05.grd
cp CITemissions_wkend_NH3_kgPerDay.grd CITemissions_wkend_vlx_NH3_kgPerDay_cb05.grd


#exit

#----------------------------------------------------------------------
#vpv:
#----------------------------------------------------------------------
cat <<eof >petEvp.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Weekday file name
${datapath}/VPV/mvems_${mm_name_veh}wkday_evaporative_NH3.in 
Weekend file name
${datapath}/VPV/mvems_${mm_name_veh}wkend_evaporative_NH3.in 
Output TAPM file generic name
emsns_gmr.vpv.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset_veh
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.953,0.283,0.635,0.083,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.808,0.000,0.000
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP?
F
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
eof

${exepath}/build_motorVehicle_emissions_glo/build_motorvehicle_emissions_glo petEvp.run >&petEvp_${mm_name}.trace
cp wkday_emissions_kgPerHour.csv wkday_emissions_vpv_geo_kgPerHour_cb05.csv
cp wkend_emissions_kgPerHour.csv wkend_emissions_vpv_geo_kgPerHour_cb05.csv

cp CITemissions_wkday_NO_kgPerDay.grd CITemissions_wkday_vpv_NO_kgPerDay_cb05.grd
cp CITemissions_wkend_NO_kgPerDay.grd CITemissions_wkend_vpv_NO_kgPerDay_cb05.grd
cp CITemissions_wkday_NO2_kgPerDay.grd CITemissions_wkday_vpv_NO2_kgPerDay_cb05.grd
cp CITemissions_wkend_NO2_kgPerDay.grd CITemissions_wkend_vpv_NO2_kgPerDay_cb05.grd
cp CITemissions_wkday_CO_kgPerDay.grd CITemissions_wkday_vpv_CO_kgPerDay_cb05.grd
cp CITemissions_wkend_CO_kgPerDay.grd CITemissions_wkend_vpv_CO_kgPerDay_cb05.grd
cp CITemissions_wkday_SO2_kgPerDay.grd CITemissions_wkday_vpv_SO2_kgPerDay_cb05.grd
cp CITemissions_wkend_SO2_kgPerDay.grd CITemissions_wkend_vpv_SO2_kgPerDay_cb05.grd
cp CITemissions_wkday_ALD2_kgPerDay.grd CITemissions_wkday_vpv_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkend_ALD2_kgPerDay.grd CITemissions_wkend_vpv_ALD2_kgPerDay_cb05.grd
cp CITemissions_wkday_PART_kgPerDay.grd CITemissions_wkday_vpv_PART_kgPerDay_cb05.grd
cp CITemissions_wkend_PART_kgPerDay.grd CITemissions_wkend_vpv_PART_kgPerDay_cb05.grd
cp CITemissions_wkday_ETH_kgPerDay.grd CITemissions_wkday_vpv_ETH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETH_kgPerDay.grd CITemissions_wkend_vpv_ETH_kgPerDay_cb05.grd
cp CITemissions_wkday_FORM_kgPerDay.grd CITemissions_wkday_vpv_FORM_kgPerDay_cb05.grd
cp CITemissions_wkend_FORM_kgPerDay.grd CITemissions_wkend_vpv_FORM_kgPerDay_cb05.grd
cp CITemissions_wkday_ISOP_kgPerDay.grd CITemissions_wkday_vpv_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkend_ISOP_kgPerDay.grd CITemissions_wkend_vpv_ISOP_kgPerDay_cb05.grd
cp CITemissions_wkday_OLE_kgPerDay.grd CITemissions_wkday_vpv_OLE_kgPerDay_cb05.grd
cp CITemissions_wkend_OLE_kgPerDay.grd CITemissions_wkend_vpv_OLE_kgPerDay_cb05.grd
cp CITemissions_wkday_PAR_kgPerDay.grd CITemissions_wkday_vpv_PAR_kgPerDay_cb05.grd
cp CITemissions_wkend_PAR_kgPerDay.grd CITemissions_wkend_vpv_PAR_kgPerDay_cb05.grd
cp CITemissions_wkday_TOL_kgPerDay.grd CITemissions_wkday_vpv_TOL_kgPerDay_cb05.grd
cp CITemissions_wkend_TOL_kgPerDay.grd CITemissions_wkend_vpv_TOL_kgPerDay_cb05.grd
cp CITemissions_wkday_XYL_kgPerDay.grd CITemissions_wkday_vpv_XYL_kgPerDay_cb05.grd
cp CITemissions_wkend_XYL_kgPerDay.grd CITemissions_wkend_vpv_XYL_kgPerDay_cb05.grd
cp CITemissions_wkday_ETOH_kgPerDay.grd CITemissions_wkday_vpv_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkend_ETOH_kgPerDay.grd CITemissions_wkend_vpv_ETOH_kgPerDay_cb05.grd
cp CITemissions_wkday_MEOH_kgPerDay.grd CITemissions_wkday_vpv_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkend_MEOH_kgPerDay.grd CITemissions_wkend_vpv_MEOH_kgPerDay_cb05.grd
cp CITemissions_wkday_UNR_kgPerDay.grd CITemissions_wkday_vpv_UNR_kgPerDay_cb05.grd
cp CITemissions_wkend_UNR_kgPerDay.grd CITemissions_wkend_vpv_UNR_kgPerDay_cb05.grd
cp CITemissions_wkday_NH3_kgPerDay.grd CITemissions_wkday_vpv_NH3_kgPerDay_cb05.grd
cp CITemissions_wkend_NH3_kgPerDay.grd CITemissions_wkend_vpv_NH3_kgPerDay_cb05.grd


#----------------------------------------------------------------------
#gse:
#----------------------------------------------------------------------
cat <<eof >comdomestic.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Number of source groups; GLOMAP?
8 F
Loop over source groups
----------------------------------------------------------
aircraft
Weekday file name 
${datapath}/aircraft/aems_${mm_name_mm}wkday_aircraft_NH3.in
Weekend file name 
${datapath}/aircraft/aems_${mm_name_mm}wkend_aircraft_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.983,0.760,0.240,0.0,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
comm-veh
Weekday file name 
${datapath}/comvehequip/aems_${mm_name_mm}wkday_comvehequip_NH3.in
Weekend file name 
${datapath}/comvehequip/aems_${mm_name_mm}wkend_comvehequip_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.970,0.643,0.237,0.120,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
ind-veh
Weekday file name 
${datapath}/indvehequip/aems_${mm_name_mm}wkday_indvehequip_NH3.in
Weekend file name 
${datapath}/indvehequip/aems_${mm_name_mm}wkend_indvehequip_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.970,0.643,0.237,0.120,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
loco
Weekday file name 
${datapath}/locomotives/aems_${mm_name_mm}wkday_Locomotives_NH3.in
Weekend file name 
${datapath}/locomotives/aems_${mm_name_mm}wkend_Locomotives_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.970,0.771,0.176,0.053,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
nonexhaustPM
Weekday file name 
${datapath}/nonexhaustpm/mvems_${mm_name_mm}wkday_nonexhaustpm_NH3.in
Weekend file name 
${datapath}/nonexhaustpm/mvems_${mm_name_mm}wkend_nonexhaustpm_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.532,0.01,0.094,0.893,0.0,0.0135,0.00
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.0,0.0,0.0
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
shipping
Weekday file name 
${datapath}/Shipping/aems_${mm_name_mm}wkday_shipping_NH3.in
Weekend file name 
${datapath}/Shipping/aems_${mm_name_mm}wkend_shipping_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx (EPA Speciate 5674)
0.920,0.008,0.191,0.156,0.0,0.645,0.00
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
other
Weekday file name 
${datapath}/other-whe/aems_${mm_name_mm}wkday_other-whe_NH3.in
Weekend file name 
${datapath}/other-whe/aems_${mm_name_mm}wkend_other-whe_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.300,0.100,0.100,0.800,0.0,-1.0,0.03
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
----------------------------------------------------------
fugitives
Weekday file name 
${datapath}/IndFug/aems_${mm_name_mm}wkday_IndFug_excl_werosion.in
Weekend file name 
${datapath}/IndFug/aems_${mm_name_mm}wkend_IndFug_excl_werosion.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx (EPA Speciate 5674)
0.150,0.005,0.1,0.9,0.0,0.001,0.00
VBS- 9 volatility bins
0.048,0.096,0.144,0.224,0.288,0.480,0.640,0.800,1.280
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.0,0.0,0.0
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5.7,1500.,1.59,0.5
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
Finish source group loop----------------------------------
Output TAPM file generic name
emsns_gmr.gse.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset
eof

${exepath}/build_commercialDomestic_emissions_glo/build_commercialdomestic_emissions_glo comdomestic.run>& comdomestic_${mm_name}.trace
#${exepath}/build_commercialDomestic_emissions_glo/build_commercialdomestic_emissions_glo comdomestic.run
#cp wkday_emissions_kgPerHour.csv wkday_emissions_gse_kgPerHour_cb05.csv
#cp wkend_emissions_kgPerHour.csv wkend_emissions_gse_kgPerHour_cb05.csv

#----------------------------------------------------------------------
#whe:
#----------------------------------------------------------------------
cat <<eof >woodheaters.run
ys, ms, ds
${yyyy} ${mm} ${dd}
ye, me, de
${yyyy} ${mm} ${de}
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Nonuniform grid? Write out .grd files
T T
Number of source groups; GLOMAP?
1 F
Loop over source groups
----------------------------------------------------------
woodheater
Weekday file name
${datapath}/whe/aems_${mm_name_whe}wkday_whe_NH3.in
Weekend file name
${datapath}/whe/aems_${mm_name_whe}wkend_whe_NH3.in
PM2.5/PM10,EC/PM10,OC/PM10,DU/PM10,LEVO/PM10,SO3/PM10,SO3/SOx
0.963,0.151,0.419,0.431,0.25,-1.0,0.03
VBS- 9 volatility bins
0.0,0.0,0.160,0.224,0.528,0.528,0.160,0.0,0.0
IOLE/OLE,ALDX/ALD2,ETHA/PAR
0.0,0.0,0.0
PTOL/TOL,PXYL/XYL,PBNZ/TOL
0.77,0.48,0.31
GLOMAP definitions
sulfate. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 2,60.,1.59,0.5, 3,150.,1.59,0.5
ec+oc. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
1, 5,37.,1.59,1.0
du. n-modes, (mode_no,d(nm),g-sig,frac, for each mode)
2, 6,150.,1.59,0.5,  7,1500.,1.59,0.5
Finish source group loop
Output TAPM file generic name
emsns_gmr.whe.bin
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset_whe
eof

${exepath}/build_commercialDomestic_emissions_glo/build_commercialdomestic_emissions_glo woodheaters.run>& woodheaters_${mm_name}.trace
#cp wkday_emissions_kgPerHour.csv wkday_emissions_whe_kgPerHour_cb05.csv
#cp wkend_emissions_kgPerHour.csv wkend_emissions_whe_kgPerHour_cb05.csv


#----------------------------------------------------------------------
#pse:
#----------------------------------------------------------------------
cat <<eof >run_coalonly_wkday.run
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Convert AMG to lat/long?
T
Name of base case pems file
${datapath}/powergen_coal/pems_${mm_name_mm}wkday_powergencoal_NH3.in
PM2.5/PM10; source number
1 1
Name of test case pems file
emsn_gmr_${mm_name_mm}wkday_powercoal.pse.bin
Include user emissions
F
Name of user emissions file
cogen_24h_emissions.csv
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset
eof
${exepath}/build_elevated_emissions_glo/build_elevated_emissions_noglo run_coalonly_wkday.run >&run_coalonly_wkday_${mm_name}.trace

#exit

cat <<eof >run_gasonly_wkday.run
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Convert AMG to lat/long?
T
Name of base case pems file
${datapath}/powergen_gas/pems_${mm_name_mm}wkday_powergengas_NH3.in
PM2.5/PM10; source number
1 2
Name of test case pems file
emsn_gmr_${mm_name_mm}wkday_powergas.pse.bin
Include user emissions
F
Name of user emissions file
cogen_24h_emissions.csv
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset
eof
${exepath}/build_elevated_emissions_glo/build_elevated_emissions_noglo run_gasonly_wkday.run >&run_gasonly_wkday_${mm_name}.trace


cat <<eof >run_rest_wkday.run
Name of citgrid.dat file
${datapath}/cit_grid_1km.dat
Convert AMG to lat/long?
T
Name of base case pems file
${datapath}/pse_other/pems_${mm_name_mm}wkday_pseOther_NH3.in
PM2.5/PM10; source number
1 3
Name of test case pems file
emsn_gmr_${mm_name_mm}wkday_other.pse.bin
Include user emissions
F
Name of user emissions file
cogen_24h_emissions.csv
Offset (t_model=t_inv+offset) .eg. EST = DST -1
$offset
eof
#${exepath}/build_elevated_emissions_glo/build_elevated_emissions_glo run_rest_wkday.run >&run_rest_wkday_${mm_name}.trace
${exepath}/build_elevated_emissions_glo/build_elevated_emissions_noglo run_rest_wkday.run >&run_rest_wkday_${mm_name}.trace

#----------------------------------------------------------------------
#psemerge:
#----------------------------------------------------------------------

cat <<eof >psemerge.run
first pse file
emsn_gmr_${mm_name_mm}wkday_powercoal.pse.bin
second pse file
emsn_gmr_${mm_name_mm}wkday_powergas.pse.bin
name of merged pse file
temp.pse.bin
number of days to process
1
eof

${exepath}/psemerge/psemerge >&psemerge_coalgas_${mm_name}.trace

cat <<eof >psemerge.run
first pse file
temp.pse.bin
second pse file
emsn_gmr_${mm_name_mm}wkday_other.pse.bin
name of merged pse file
emsn_gmr_${mm_name_mm}wkday_anthropogenic.pse.bin
number of days to process
1
eof
${exepath}/psemerge/psemerge >&psemerge_coalgasother_${mm_name}.trace

tar -cvf emission_outputs_${yyyy}${mm}.tar --remove-files CITemissions* wk*.csv *.grd
