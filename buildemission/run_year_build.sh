#!/bin/bash
#SBATCH -J bldPM25
#SBATCH -t 24:00:00
##SBATCH --mem-per-cpu 16384
##SBATCH --cpus-per-task 72
#SBATCH -o Output-%j
#SBATCH -e Error-%j
#SBATCH -p SLES15
##SBATCH -w irscomp11
#SBATCH -N 1
#SBATCH --ntasks-per-node 1

#-------------------------------yyyy-m-mmm-ds-tz-ed
./buildemissions_fug_allpm25.sh 2013 1 jan 1 -10 31 
./buildemissions_fug_allpm25.sh 2013 2 feb 1 -10 28
./buildemissions_fug_allpm25.sh 2013 3 mar 1 -10 31
./buildemissions_fug_allpm25.sh 2013 4 apr 1 -10 30
./buildemissions_fug_allpm25.sh 2013 5 may 1 -10 31
./buildemissions_fug_allpm25.sh 2013 6 jun 1 -10 30
./buildemissions_fug_allpm25.sh 2013 7 jul 1 -10 31
./buildemissions_fug_allpm25.sh 2013 8 aug 1 -10 31
./buildemissions_fug_allpm25.sh 2013 9 sep 1 -10 30
./buildemissions_fug_allpm25.sh 2013 10 oct 1 -10 31
./buildemissions_fug_allpm25.sh 2013 11 nov 1 -10 30
./buildemissions_fug_allpm25.sh 2013 12 dec 1 -10 31

