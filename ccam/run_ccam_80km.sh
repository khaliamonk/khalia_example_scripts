#!/bin/bash
#SBATCH --job-name=nrd_80km
#SBATCH -p CAS
#SBATCH --mem=96gb
#SBATCH -N 1
#SBATCH --ntasks-per-node=72
#SBATCH -w irscomp11
##SBATCH --cores-per-socket=10
#SBATCH --time=24:00:00

export OMPI_MCA_fcoll=dynamic
#export OMPI_MCA_btl_base_verbose=30
#export OMPI_MCA_orte_base_help_aggregate=0 
#10G network only
#export OMPI_MCA_btl_tcp_if_include=192.168.144.1/23
# eth0 only
#export OMPI_MCA_btl_tcp_if_include=eth0
#IB network only
export OMPI_MCA_btl_tcp_if_include=10.0.1.1/24
###############################################################
# This is the CCAM run script

rm year.qm
###############################################################
# MODULES

#module load mpt            # MPI
#module load netcdf/4.3.3.1 # NetCDF
#module load python         # Python
source /mnt/appsource/modules/current/init/bash
module use /home/barthelemyx/modulefiles/SLES15
module load InitModule
ulimit -s 350000
###############################################################
# Specify parameters

hdir=/mnt/climate/cas/scratch/monkk/ccam/scripts/nrd_ndg      # script directory
wdir=$hdir/wdir                              # working directory
machinetype=0                                # machine type (0=mpirun, 1=srun)
nproc=$SLURM_NTASKS                          # number of processors

midlon=134.33                                   # central longitude of domain
midlat=-34.36                                    # central latitude of domain
gridres=80                                # required resolution (km) of domain (-999.=global)
gridsize=48                                  # cubic grid size (e.g., 48, 72, 96, 144, 192, 288, 384, 576, 768, etc)
iys=2012                                     # start year
ims=12                                        # start month
iye=2013                                     # end year
ime=12                                       # end month
leap=1                                       # use leap days (0=off, 1=on)
ncountmax=13                                 # number of months before resubmit

name=ccam_${gridres}km                       # run name
if [[ $gridres = "-999." ]]; then
  gridtxt=$(echo "scale=1; 112.*90./$gridsize" | bc -l)
  name=`echo $name | sed "s/$gridres/$gridtxt"/g`
fi

ncout=3                                      # standard output format (0=none, 1=CCAM, 2=CORDEX, 3=CTM-tar, 4=Nearest, 5=CTM-raw, 6=CORDEX-surface)
nctar=1                                      # TAR output files in OUTPUT directory (0=off, 1=on, 2=delete)
ktc=360                                      # standard output period (mins)
minlat=-60.36                                 # output min latitude (degrees) (-9999.=automatic)
maxlat=-8.36                                 # output max latitude (degrees) (-999.=automatic)
minlon=104.33                                 # output min longitude (degrees) (-999.=automatic)
maxlon=164.33                                 # output max longitude (degrees) (-999.=automatic)
reqres=-999.                                 # required output resolution (degrees) (-999.=automatic)
outlevmode=1                                 # output mode for levels (0=pressure, 1=meters)
plevs="1000, 850, 700, 500, 300"             # output pressure levels (hPa) for outlevmode=0
mlevs="10,20, 50, 100, 150, 250, 500, 750, 1000,2000, 3000, 4000, 8000, 12000, 16000, 20000" # output height levels (m) for outlevmode=1
dlevs="5, 10, 50, 100, 500, 1000, 5000"      # ocean depth levels (m)
ncsurf=0                                     # high-freq output (0=none, 1=lat/lon, 2=raw)
ktc_surf=60                                  # high-freq file output period (mins)

dmode=0                                      # simulation type (0=downscale spectral(GCM), 1=SST-only, 2=downscale spectral(CCAM), 3=SST-6hr )
cmip=cmip5                                   # CMIP scenario (CMIP3 or CMIP5)
rcp=RCP45                                    # RCP scenario (historic, RCP45 or RCP85)
mlev=35                                      # number of model levels (27, 35, 54, 72, 108 or 144)
sib=2                                        # land surface (1=CABLE, 2=MODIS, 3=CABLE+SLI)
aero=1                                       # aerosols (0=off, 1=prognostic)
conv=1                                       # convection (0=2014, 1=2015a, 2=2015b, 3=2017)
cloud=1                                      # cloud microphysics (0=liq+ice, 1=liq+ice+rain, 2=liq+ice+rain+snow+graupel)
bmix=0                                       # boundary layer (0=Ri, 1=TKE-eps)
mlo=0                                        # ocean (0=Interpolated SSTs, 1=Dynamical ocean)
casa=0                                       # CASA-CNP carbon cycle with prognostic LAI (0=off, 1=CASA-CNP, 2=CASA-CN+POP, 3=CASA-CN+POP+CLIM)

# User defined parameters.  Delete $hdir/vegdata to update.
uclemparm=default                            # urban parameter file (default for standard values)
cableparm=default                            # CABLE vegetation parameter file (default for standard values)
soilparm=default                             # soil parameter file (default for standard values)
vegindex=default                             # Define vegetation indices for user vegetation (default for standard values)
uservegfile=none                             # User specified vegetation map (none for no user file)
userlaifile=none                             # User specified LAI map (none for no user file)

###############################################################
# Host atmosphere for dmode=0, dmode=2 or dmode=3

bcdom=ccam_eraint_                           # host file prefix for dmode=0, dmode=2 or dmode=3
bcdir=/mnt/load/CAS/ERA_int                 # host atmospheric data (dmode=0, dmode=2 or dmode=3)
bcsoil=0                                     # use climatology for initial soil moisture (0=constant, 1=climatology)

###############################################################
# Sea Surface Temperature for dmode=1

sstfile=ACCESS1-0_RCP45_bcvc_osc_ots_santop96_18_0.0_0.0_1.0.nc # sst file for dmode=1
sstinit=$bcdir/$bcdom$iys$ims.nc                                # initial conditions file for dmode=1
sstdir=/mnt/climate/cas/scratch/monkk/ccam/gcmsst                                 # SST data (dmode=1)

###############################################################
# Specify directories and executables

insdir=/mnt/climate/cas/scratch/monkk/ccam                     # install directory
excdir=$insdir/scripts/nrd_ndg             # location of run_ccam.py
stdat=$insdir/ccamdata                       # eigen and radiation datafiles
terread=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/terread_sles15_20190514
igbpveg=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/igbpveg_sles15_20190514
sibveg=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/sibveg_sles15_20190514
ocnbath=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/ocnbath_sles15_20190514
casafield=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/casafield_sles15_20190514
smclim=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/smclim
aeroemiss=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/aeroemiss_sles15_20190514
model=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/globpea_opt_netcdf3_no_omp_sles15_ompi4_slurm17_20190708 
pcc2hist=/home/barthelemyx/Projects/aq-ccam/Repository/BIN/pcc2hist_sles15_ompi4_slurm17_20190514

###############################################################

python $excdir/run_ccam.py --name $name --nproc $nproc --midlon " $midlon" --midlat " $midlat" --gridres " $gridres" \
                   --gridsize $gridsize --mlev $mlev --iys $iys --ims $ims --iye $iye --ime $ime --leap $leap \
                   --ncountmax $ncountmax --ktc $ktc --minlat " $minlat" --maxlat " $maxlat" --minlon " $minlon" \
                   --maxlon " $maxlon" --reqres " $reqres" --outlevmode $outlevmode --plevs ${plevs// /} \
		   --mlevs ${mlevs// /} --dlevs ${dlevs// /} --dmode $dmode \
                   --sib $sib --aero $aero --conv $conv --cloud $cloud --bmix $bmix --mlo $mlo \
                   --casa $casa --ncout $ncout --nctar $nctar --ncsurf $ncsurf --ktc_surf $ktc_surf \
                   --machinetype $machinetype --bcdom $bcdom --bcsoil $bcsoil \
                   --sstfile $sstfile --sstinit $sstinit --cmip $cmip --rcp $rcp --insdir $insdir --hdir $hdir \
                   --wdir $wdir --bcdir $bcdir --sstdir $sstdir --stdat $stdat \
                   --aeroemiss $aeroemiss --model $model --pcc2hist $pcc2hist --terread $terread --igbpveg $igbpveg \
                   --sibveg $sibveg --ocnbath $ocnbath --casafield $casafield --smclim $smclim \
		   --uclemparm $uclemparm --cableparm $cableparm --soilparm $soilparm --vegindex $vegindex \
		   --uservegfile $uservegfile --userlaifile $userlaifile

if [ "`cat $hdir/restart.qm`" == "True" ]; then
  echo 'Restarting script'
  rm $hdir/restart.qm
  sbatch $hdir/run_ccam_80km.sh
elif [ "`cat $hdir/restart.qm`" == "Complete" ]; then
  echo 'CCAM simulation completed normally'
  rm $hdir/restart.qm
fi

exit
