#!/bin/bash
#SBATCH -J prepJAN
#SBATCH -t 48:00:00
#SBATCH --cpus-per-task 72
#SBATCH -o Output-pJAN.%j
#SBATCH -e Error-pJAN.%j
#SBATCH -p CAS
#SBATCH -x irscomp12
#SBATCH -N 1
#SBATCH --ntasks-per-node 1

source /mnt/appsource/modules/current/init/bash
module purge
module use /home/barthelemyx/modulefiles/SLES15
module load InitModule
module list
ulimit -s 350000

##!/bin/tcsh
##source /mnt/scratch2/CAS/models/ccam-ctm/load_env.csh
##PBS -l nodes=1:ppn=1
##PBS -l cput=10:00:00
##PBS -l walltime=10:00:00
##PBS -l vmem=4000mb
##PBS -N p_jan_s18
##PBS -j oe 
#limit stacksize 1000000
#
# Script to run TAPM-CTM preprocessor

##############################################
# scenarios and options
##############################################
export MONTH=jan
export CHEM=cb05_aer2
export SCENARIO=${MONTH}13
export SIOA=ISORROPIA_IIa
export SOA=VBS2
export TRANSPORT=WALCEK
export DIFF=noNWPDIFFUSIVITY
export BIO=$MONTH
export YEAR=2013
export EXE=/mnt/climate/cas/project/CTM/software/August-16/BIN/

export METHOD=allpm25
export CASE=S1a_prep

#cd ~/ccam-ctm/
cd /mnt/climate/cas/scratch/NRDiesel/ctm/${CASE}/output/${MONTH}
##############################################
#start date; end date
# used by CTM
##############################################
export YEAR_S=2013
export MONTH_S=01
export DAY_S=01

export YEAR_EMISSIONS=$YEAR_S
export MONTH_EMISSIONS=$MONTH
##############################################
# simulation hours
##############################################
export DAY_N=31
((HOUR_N =$DAY_N* 24))
echo $HOUR_N

##############################################
# Domains
##############################################
export NX_D1=85
export NY_D1=74
export SW_LON_D1=104.33 
export SW_LAT_D1=-60.36
export res_D1=0.8
export DOM_NAME_D1=aus

export NX_D2=62
export NY_D2=62
export SW_LON_D2=140.574 
export SW_LAT_D2=-42.7953 
export res_D2=0.27
export DOM_NAME_D2=nsw

export NX_D3=99
export NY_D3=99
export SW_LON_D3=149.634
export SW_LAT_D3=-34.8153
export res_D3=0.03
export DOM_NAME_D3=gmr

export NX_D4=54
export NY_D4=54
export SW_LON_D4=149.634
export SW_LAT_D4=-31.8453
export res_D4=0.03
export DOM_NAME_D4=nmi

export PAUS=E${SW_LON_D1}_N${SW_LAT_D1}_${NX_D1}nx_${NY_D1}ny_${res_D1}
export PNSW=E${SW_LON_D2}_N${SW_LAT_D2}_${NX_D2}nx_${NY_D2}ny_${res_D2}
export PGMR=E${SW_LON_D3}_N${SW_LAT_D3}_${NX_D3}nx_${NY_D3}ny_${res_D3}
export PNMI=E${SW_LON_D4}_N${SW_LAT_D4}_${NX_D4}nx_${NY_D4}ny_${res_D4}

export GSE_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.gse.bin
export VPX_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.vpx.bin
export VDX_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.vdx.bin
export VPV_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.vpv.bin
export VLX_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.vlx.bin
export WHE_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/2013_${MONTH}_emsns_gmr.whe.bin
export PSE_EMIS=/mnt/climate/cas/project/NRDiesel/models/buildemission/${METHOD}/emsn_gmr_${SCENARIO}wkday_anthropogenic.pse.bin

##############################################
# Inventory scenarios
##############################################
#dmget /cs/datastore/csdar/cop043/future_air/inventory/$YEAR_EMISSIONS/$MONTH_EMISSIONS/TAPM_*.bin
#dmget /home/cMAR/cop043/syd_particle_study/emissions/$YEAR_EMISSIONS/$MONTH_EMISSIONS/TAPM_*.bin

#goto ctm_config


##############################################
# copy local file into directory
##############################################
export COMMONFILES_DIR=/mnt/climate/cas/project/NRDiesel/models/commonfiles

ln -sf ${COMMONFILES_DIR}/lineSegments.csv 
ln -sf ${COMMONFILES_DIR}/locations.csv 
ln -sf ${COMMONFILES_DIR}/KMedited/tcbc_cb05_aer2_${MONTH}.in tcbc_cb05_aer2.in
ln -sf ${COMMONFILES_DIR}/nsw_biogenic_def.txt 
ln -sf ${COMMONFILES_DIR}/SOIL/paus/ccam_${PAUS}_soil 
ln -sf ${COMMONFILES_DIR}/SOIL/nnsw/ccam_${PNSW}_soil 
ln -sf ${COMMONFILES_DIR}/SOIL/ngmr/ccam_${PGMR}_soil 
ln -sf ${COMMONFILES_DIR}/SOIL/nnmi/ccam_${PNMI}_soil 
ln -sf ${COMMONFILES_DIR}/soil_param 
ln -sf ${COMMONFILES_DIR}/soil_psd 
ln -sf ${COMMONFILES_DIR}/LAI/paus/ccam_${PAUS}_lai_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnsw/ccam_${PNSW}_lai_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/ngmr/ccam_${PGMR}_lai_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnmi/ccam_${PNMI}_lai_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/paus/ccam_${PAUS}_laiG_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnsw/ccam_${PNSW}_laiG_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/ngmr/ccam_${PGMR}_laiG_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnmi/ccam_${PNMI}_laiG_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/paus/ccam_${PAUS}_laiW_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnsw/ccam_${PNSW}_laiW_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/ngmr/ccam_${PGMR}_laiW_${MONTH} 
ln -sf ${COMMONFILES_DIR}/LAI/nnmi/ccam_${PNMI}_laiW_${MONTH} 
ln -sf ${COMMONFILES_DIR}/NO2_05dec2006.txt 
ln -sf ${COMMONFILES_DIR}/NO3NO_05dec2006.txt 
ln -sf ${COMMONFILES_DIR}/NO3_NO2_05dec2006.txt 
ln -sf ${COMMONFILES_DIR}/O3O1D_05dec2006.txt 
ln -sf ${COMMONFILES_DIR}/O3_O3P_05dec2006.txt 
ln -sf ${COMMONFILES_DIR}/C*.txt .
ln -sf ${COMMONFILES_DIR}/H*.txt .
ln -sf ${COMMONFILES_DIR}/N*.txt .
ln -sf ${COMMONFILES_DIR}/I*.txt .
ln -sf ${COMMONFILES_DIR}/G*.txt .
ln -sf ${COMMONFILES_DIR}/M*.txt .
ln -sf ${COMMONFILES_DIR}/D*.txt .
ln -sf ${COMMONFILES_DIR}/P*.txt .
ln -sf ${COMMONFILES_DIR}/A*.txt .
ln -sf ${COMMONFILES_DIR}/Jbase_profiles.dat 


##############################################
# Generate data packet
##############################################
#prep:
rm -f ctm_prep_init.inp
cat <<eof >ctm_prep_init.inp
Mode of TAPM-CTM run: online offline rerun: mode
off
Mode of CTM run: warm cold : start_mode
cold
Scenario of run: scenario
$SCENARIO
Number of days to run: ndays
$DAY_N
Date of start day,yyyy,mm,dd,timezone
$YEAR_S $MONTH_S $DAY_S 0
Start/End times for all ngrids in LST: shh,ehh
 0 23
TAPM info: ngrid,xc,yc,lonc,latc,nland  ! NO used for CCAM run-Perhaps  except nland(check later)
 1 330000.000 6251000.00 151.158340 -33.8666649 39
Number of CTM grids: ctm_ngrid
4
Name of airshed: airshed
${DOM_NAME_D1}
Name of airshed: airshed
${DOM_NAME_D2}
Name of airshed: airshed
${DOM_NAME_D3}
Name of airshed: airshed
${DOM_NAME_D4}
Postion of SW corner of each grid:xlong,ylat
$SW_LON_D1 $SW_LAT_D1
EACH GRID HORIZONTAL DIMENSION: NX,NY
$NX_D1 $NY_D1
EACH GRID HORIZONTAL RESOLUTION: HRESX,HRESY (deg) , for *NC naming
$res_D1 $res_D1
Postion of SW corner of each grid:xlong,ylat
$SW_LON_D2 $SW_LAT_D2
EACH GRID HORIZONTAL DIMENSION: NX,NY
$NX_D2 $NY_D2
EACH GRID HORIZONTAL RESOLUTION: HRESX,HRESY (deg) , for *NC naming
$res_D2 $res_D2
Postion of SW corner of each grid:xlong,ylat
$SW_LON_D3 $SW_LAT_D3 
EACH GRID HORIZONTAL DIMENSION: NX,NY
$NX_D3 $NY_D3
EACH GRID HORIZONTAL RESOLUTION: HRESX,HRESY (deg) , for *NC naming
$res_D3 $res_D3
Postion of SW corner of each grid:xlong,ylat
$SW_LON_D4 $SW_LAT_D4 
EACH GRID HORIZONTAL DIMENSION: NX,NY
$NX_D4 $NY_D4
EACH GRID HORIZONTAL RESOLUTION: HRESX,HRESY (deg) , for *NC naming
$res_D4 $res_D4
EACH GRID VERTICAL DIMENSION: NZ
 16 16 16 16
ALL GRID CELL HEIGHT: pstar
0.997039,0.991134,0.982316,0.970636,0.959042,0.947535,0.930436,0.907940
0.885787,0.863977,0.831901,0.780135,0.682876,0.593808,0.512701,0.439293
Number of vertical levels to write out: NZ_write
 1
PATH for input netcdf files: IN_PATH
./
PATH for output/SPStest: OUT_PATH
./
CIT or TAPM format emissions
TAPM
Switches for TAPM source groups 'vpx','vdx','vlx','vpv','gse','whe','pse'
1 1 1 1 1 1 1
For each grid list names of selected source groups
${VPX_EMIS}
${VPX_EMIS}
${VPX_EMIS}
${VPX_EMIS}
${VDX_EMIS}
${VDX_EMIS}
${VDX_EMIS}
${VDX_EMIS}
${VLX_EMIS}
${VLX_EMIS}
${VLX_EMIS}
${VLX_EMIS}
${VPV_EMIS}
${VPV_EMIS}
${VPV_EMIS}
${VPV_EMIS}
${GSE_EMIS}
${GSE_EMIS}
${GSE_EMIS}
${GSE_EMIS}
${WHE_EMIS}
${WHE_EMIS}
${WHE_EMIS}
${WHE_EMIS}
${PSE_EMIS}
${PSE_EMIS}
${PSE_EMIS}
${PSE_EMIS}
EACH TIMESTEP FOR ICBC: icbc_int gives icbc_step
 1 900 900 900 900
EACH GRID TIMESTEP FOR WRITING OUT: WRT_intsec(NGRID) in sec
 3600 3600 3600 3600
ENTER STORE DEPOSITION TUNRED ON or OFF
F   !T
NUMBER of SPECIES to STORE DEPOSTION MASS
 3
NAME of  SPECIES FOR DRY/WET DEPOSITION MASS STORED
hg0 rgm hgp
UNIT of  DEPOSITION MASS
ugm2 ugm2 ugm2
STARTOPTIONS
storeBIO
storeDust
storeAOD
storeSalt
ENDOPTIONS

eof

cp ${EXE}/ctm_prep_init_${CHEM}_opt_SLES15.x64 .
./ctm_prep_init_${CHEM}_opt_SLES15.x64 ctm_prep_init.inp

##############################################
# set up ctm_config files
##############################################
# meteo_config
rm -f gen_meteo_config.txt
cat <<eof >gen_meteo_config.txt
Number of day, archives per day
$DAY_N, 25
3-D data stored in individual hourly files? (T/F)
F
Time interval of 3-D data stored
1
2-D file name
NONE
3-D file name
./
FILE prefix
ccam_
ccam start date
$YEAR_S $MONTH_S $DAY_S 00
eof

##############################################
#ctm_config: DOMAIN 1 - AUS
rm -f ctm_config_0.txt
cat <<eof >ctm_config_0.txt
Name of scenario: base
$SCENARIO
Name of chemistry scheme: 
$CHEM
Start date and time for CTM run (yyyy,mm,dd,hh)
$YEAR_S $MONTH_S $DAY_S 0
Number of hours of integration
$HOUR_N 
Number of CTM master/inline grids: ng
 1
Master/inline grids - 0=master or i=inline
 0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D1}
Master/inline grids - SW corner: ylat, xlong
$SW_LAT_D1 $SW_LON_D1
Master/inline grids - horiz. dimension: nx,ny
$NX_D1 $NY_D1
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D1 $res_D1
Master/inline grids - vertical dimension: nz
 16
Number of CTM nested master grids: ctm_offline : nosg
 1
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D2}
Master/inline grids - SW corner: y,x (deg)
$SW_LAT_D2 $SW_LON_D2
Master/inline grids - horiz. dimension: nx,ny
$NX_D2 $NY_D2
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D2 $res_D2
Nested master grids - vertical dimension: nz
16
COORDINATE SYSTEM
spherical
ALL GRID VERTICAL COORDINATE SYSTEM
CCAM-SIGMA
Grid system: Arakawa grid class: grid_sys
A
ALL GRID CELL HEIGHT: in Hpascal temporaraily put up high as CCAM (.9974  to .9954)
0.997039,0.991134,0.982316,0.970636,0.959042,0.947535,0.930436,0.907940
0.885787,0.863977,0.831901,0.780135,0.682876,0.593808,0.512701,0.439293
EACH GRID VECTOR LENGTHS FOR X-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Y-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Z-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR CHEMISTRY
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 16
EACH GRID TIMESTEP FOR WHICH MET IS CONSTANT
 300
EACH GRID TIMESTEP FOR OPERATOR SPLITTING
 150
EACH GRID TIMESTEP output/SPStest: wrt_step(ng) (s)
 3600
STARTOPTIONS
WHE_2
-6.5
ANTHROPOGENIC
MOTOR_VEHICLE
COM_DOM
INDUSTRIAL
$TRANSPORT
ADVECTION
HDIFFUSION
VDIFFUSION
LOCKSTEP
$DIFF
CHEMISTRY
NONITRATE
NOMARS
$SIOA
$SOA
NOCLOUDCHEMISTRY
DRYDEPOSITION
WETDEPOSITION
BIOGENIC
TREES
GRASS
noNATURALMERCURY
noNATURALWRITEHOURLY
USERBIOGENIC
nsw_biogenic_def.txt
BIOGENICLAI
ccam_${PAUS}_laiW_${MONTH}  ccam_${PAUS}_laiG_${MONTH}
noMERCURYSOIL
noausHg025.txt
SALT
SURF
20.
NOFIRE
DUST
PARTSED
AREASOURCEZ
3,3
whe,1,3,0.00,0.50,0.50
gse,1,3,0.33,0.33,0.33
vpx,1,3,0.64,0.27,0.09
NOSTACKTIPDOWNWASH
PLUMESMEAR
NOBRIGGSPLUMERISE
SCALE_MOTOR_VEHICLES (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_COMMERCIAL_DOMESTIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_INDUSTRIAL (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_FIRE (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_DUST (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_NATURAL_BIOGENIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
NOSEABREEZECAP
NOVDIFFCIT
NOPBLCIT
NOSURFACEMET
NOWARM_START
NEST
NOWRITE_WARM_START
NOMET_INTERPOLATE
NOVECTOR
AODGC
4
ASO4,AS10,NIT,NH4
1.0,1.0,1.0,1.0
2
EC25,EC10
1.0,1.0
10
APA1,APA2,APA3,APA4,APA5,APA6,APA7,APA8,APA9,OC10
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
15
AOA1,AOA2,AOA3,AOA4,AOA5,AOA6,AOA7,AOA8,BOA1,BOA2,BOA3,BOA4,BOA5,BOA6,OOA
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
1
SS25
0.0
2
SS25 SS10
1.0 1.0
2
DU02,OT25
1.0,1.0
6
DU05,DU10,DU20,DU40,DU60,OT10
1.0,1.0,1.0,1.0,1.0,1.0
ENDOPTIONS
Mixing depth limits: hmin_urban,hmin_rural,hmax
80. 30. 2000.
Logical flag for urban stability
F F
Model code (DARLAM/LAPS/TAPM): NWP_code
CCAM
Only For CCAM run-- use 2d surface info from CCAm output/SPStest(T) or use from pre-processed CDF
T
Invert vertical arrays (T/F)
F
Path of preprocessed template netcdf file names
./
soil psd data file
soil_psd
soil parameter data file
soil_param
drought factor- revised
1. 0.2
Grid points per ctm grid point
1
Soiltype and LAI files for each grid
ccam_${PAUS}_soil
ccam_${PAUS}_lai_${MONTH}
eof

##############################################
# DOMAIN 2 - NSW
rm -f ctm_config_1.txt
cat <<eof >ctm_config_1.txt
Name of scenario: base
$SCENARIO
Name of chemistry scheme: 
$CHEM
Start date and time for CTM run (yyyy,mm,dd,hh)
$YEAR_S $MONTH_S $DAY_S 0
Number of hours of integration
$HOUR_N 
Number of CTM master/inline grids: ng
 1
Master/inline grids - 0=master or i=inline
 0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D2}
Master/inline grids - SW corner: ylat, xlong
$SW_LAT_D2 $SW_LON_D2
Master/inline grids - horiz. dimension: nx,ny
$NX_D2 $NY_D2
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D2 $res_D2
Master/inline grids - vertical dimension: nz
 16
Number of CTM nested master grids: ctm_offline : nosg
 2
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D3}
${DOM_NAME_D4}
Master/inline grids - SW corner: y,x (deg)
$SW_LAT_D3 $SW_LON_D3
$SW_LAT_D4 $SW_LON_D4
Master/inline grids - horiz. dimension: nx,ny
$NX_D3 $NY_D3
$NX_D4 $NY_D4
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D3 $res_D3
$res_D4 $res_D4
Nested master grids - vertical dimension: nz
16
16
COORDINATE SYSTEM
spherical
ALL GRID VERTICAL COORDINATE SYSTEM
CCAM-SIGMA
Grid system: Arakawa grid class: grid_sys
A
ALL GRID CELL HEIGHT: in Hpascal temporaraily put up high as CCAM (.9974  to .9954)
0.997039,0.991134,0.982316,0.970636,0.959042,0.947535,0.930436,0.907940
0.885787,0.863977,0.831901,0.780135,0.682876,0.593808,0.512701,0.439293
EACH GRID VECTOR LENGTHS FOR X-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Y-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Z-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR CHEMISTRY
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 16
EACH GRID TIMESTEP FOR WHICH MET IS CONSTANT
 300
EACH GRID TIMESTEP FOR OPERATOR SPLITTING
 150
EACH GRID TIMESTEP output/SPStest: wrt_step(ng) (s)
 3600
STARTOPTIONS
WHE_2
-6.5
ANTHROPOGENIC
MOTOR_VEHICLE
COM_DOM
INDUSTRIAL
$TRANSPORT
ADVECTION
HDIFFUSION
VDIFFUSION
LOCKSTEP
$DIFF
CHEMISTRY
NONITRATE
NOMARS
$SIOA
$SOA
NOCLOUDCHEMISTRY
DRYDEPOSITION
WETDEPOSITION
BIOGENIC
TREES
GRASS
noNATURALMERCURY
noNATURALWRITEHOURLY
USERBIOGENIC
nsw_biogenic_def.txt
BIOGENICLAI
ccam_${PNSW}_laiW_${MONTH}  ccam_${PNSW}_laiG_${MONTH}
noMERCURYSOIL
noausHg025.txt
SALT
SURF
20.
NOFIRE
AREASOURCEZ
3,3
whe,1,3,0.00,0.50,0.50
gse,1,3,0.33,0.33,0.33
vpx,1,3,0.64,0.27,0.09
NOSTACKTIPDOWNWASH
PLUMESMEAR
NOBRIGGSPLUMERISE
SCALE_MOTOR_VEHICLES (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_COMMERCIAL_DOMESTIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_INDUSTRIAL (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_FIRE (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_DUST (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_NATURAL_BIOGENIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
NOSEABREEZECAP
NOVDIFFCIT
NOPBLCIT
NOSURFACEMET
NOWARM_START
NEST
NOWRITE_WARM_START
NOMET_INTERPOLATE
NOVECTOR
AODGC
4
ASO4,AS10,NIT,NH4
1.0,1.0,1.0,1.0
2
EC25,EC10
1.0,1.0
10
APA1,APA2,APA3,APA4,APA5,APA6,APA7,APA8,APA9,OC10
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
15
AOA1,AOA2,AOA3,AOA4,AOA5,AOA6,AOA7,AOA8,BOA1,BOA2,BOA3,BOA4,BOA5,BOA6,OOA
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
1
SS25
0.0
2
SS25 SS10
1.0 1.0
2
DU02,OT25
1.0,1.0
6
DU05,DU10,DU20,DU40,DU60,OT10
1.0,1.0,1.0,1.0,1.0,1.0
ENDOPTIONS

Mixing depth limits: hmin_urban,hmin_rural,hmax
80. 30. 2000.
Logical flag for urban stability
F F
Model code (DARLAM/LAPS/TAPM): NWP_code
CCAM
Only For CCAM run-- use 2d surface info from CCAm output/SPStest(T) or use from pre-processed CDF
T
Invert vertical arrays (T/F)
F
Path of preprocessed template netcdf file names
./
soil psd data file
soil_psd
soil parameter data file
soil_param
drought factor- revised
1. 0.2
Grid points per ctm grid point
1
Soiltype and LAI files for each grid
ccam_${PNSW}_soil
ccam_${PNSW}_lai_${MONTH}
eof

#############################################################
# DOMAIN 3 - GMR
rm -f ctm_config_2.txt
cat <<eof >ctm_config_2.txt
Name of scenario: base
$SCENARIO
Name of chemistry scheme: 
$CHEM
Start date and time for CTM run (yyyy,mm,dd,hh)
$YEAR_S $MONTH_S $DAY_S 0
Number of hours of integration
$HOUR_N 
Number of CTM master/inline grids: ng
 1
Master/inline grids - 0=master or i=inline
 0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D3}
Master/inline grids - SW corner: ylat, xlong
$SW_LAT_D3 $SW_LON_D3
Master/inline grids - horiz. dimension: nx,ny
$NX_D3 $NY_D3
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D3 $res_D3
Master/inline grids - vertical dimension: nz
 16
Number of CTM nested master grids: ctm_offline : nosg
0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D3}
Master/inline grids - SW corner: y,x (deg)
$SW_LAT_D3 $SW_LON_D3
Master/inline grids - horiz. dimension: nx,ny
$NX_D3 $NY_D3
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D3 $res_D3
Nested master grids - vertical dimension: nz
 16
COORDINATE SYSTEM
spherical
ALL GRID VERTICAL COORDINATE SYSTEM
CCAM-SIGMA
Grid system: Arakawa grid class: grid_sys
A
ALL GRID CELL HEIGHT: in Hpascal temporaraily put up high as CCAM (.9974  to .9954)
0.997039,0.991134,0.982316,0.970636,0.959042,0.947535,0.930436,0.907940
0.885787,0.863977,0.831901,0.780135,0.682876,0.593808,0.512701,0.439293
EACH GRID VECTOR LENGTHS FOR X-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Y-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Z-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR CHEMISTRY
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 16
EACH GRID TIMESTEP FOR WHICH MET IS CONSTANT
 300
EACH GRID TIMESTEP FOR OPERATOR SPLITTING
 150
EACH GRID TIMESTEP output/SPStest: wrt_step(ng) (s)
 3600
STARTOPTIONS
WHE_2
-6.5
ANTHROPOGENIC
MOTOR_VEHICLE
COM_DOM
INDUSTRIAL
$TRANSPORT
ADVECTION
HDIFFUSION
VDIFFUSION
LOCKSTEP
$DIFF
CHEMISTRY
NONITRATE
NOMARS
$SIOA
$SOA
NOCLOUDCHEMISTRY
DRYDEPOSITION
WETDEPOSITION
BIOGENIC
TREES
GRASS
noNATURALMERCURY
noNATURALWRITEHOURLY
USERBIOGENIC
nsw_biogenic_def.txt
BIOGENICLAI
ccam_${PGMR}_laiW_${MONTH}  ccam_${PGMR}_laiG_${MONTH}
noMERCURYSOIL
noausHg025.txt
SALT
SURF
20.
NOFIRE
AREASOURCEZ
3,3
whe,1,3,0.00,0.50,0.50
gse,1,3,0.33,0.33,0.33
vpx,1,3,0.64,0.27,0.09
NOSTACKTIPDOWNWASH
PLUMESMEAR
NOBRIGGSPLUMERISE
SCALE_MOTOR_VEHICLES (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_COMMERCIAL_DOMESTIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_INDUSTRIAL (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_FIRE (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_DUST (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_NATURAL_BIOGENIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
NOSEABREEZECAP
NOVDIFFCIT
NOPBLCIT
NOSURFACEMET
NOWARM_START
NEST
NOWRITE_WARM_START
NOMET_INTERPOLATE
NOVECTOR
AODGC
4
ASO4,AS10,NIT,NH4
1.0,1.0,1.0,1.0
2
EC25,EC10
1.0,1.0
10
APA1,APA2,APA3,APA4,APA5,APA6,APA7,APA8,APA9,OC10
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
15
AOA1,AOA2,AOA3,AOA4,AOA5,AOA6,AOA7,AOA8,BOA1,BOA2,BOA3,BOA4,BOA5,BOA6,OOA
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
1
SS25
0.0
2
SS25 SS10
1.0 1.0
2
DU02,OT25
1.0,1.0
6
DU05,DU10,DU20,DU40,DU60,OT10
1.0,1.0,1.0,1.0,1.0,1.0
ENDOPTIONS
Mixing depth limits: hmin_urban,hmin_rural,hmax
80. 30. 2000.
Logical flag for urban stability
T F
Model code (DARLAM/LAPS/TAPM): NWP_code
CCAM
Only For CCAM run-- use 2d surface info from CCAm output/SPStest(T) or use from pre-processed CDF
T
Invert vertical arrays (T/F)
F
Path of preprocessed template netcdf file names
./
soil psd data file
soil_psd
soil parameter data file
soil_param
drought factor- revised
1. 0.2
Grid points per ctm grid point
1
Soiltype and LAI files for each grid
ccam_${PGMR}_soil
ccam_${PGMR}_lai_${MONTH}
eof

#############################################################
# DOMAIN 4 - NAMOI
rm -f ctm_config_3.txt
cat <<eof >ctm_config_3.txt
Name of scenario: base
$SCENARIO
Name of chemistry scheme: 
$CHEM
Start date and time for CTM run (yyyy,mm,dd,hh)
$YEAR_S $MONTH_S $DAY_S 0
Number of hours of integration
$HOUR_N 
Number of CTM master/inline grids: ng
 1
Master/inline grids - 0=master or i=inline
 0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D4}
Master/inline grids - SW corner: ylat, xlong
$SW_LAT_D4 $SW_LON_D4
Master/inline grids - horiz. dimension: nx,ny
$NX_D4 $NY_D4
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D4 $res_D4
Master/inline grids - vertical dimension: nz
 16
Number of CTM nested master grids: ctm_offline : nosg
0
Master/inline grids - name of airsheds: airshed
${DOM_NAME_D4}
Master/inline grids - SW corner: y,x (deg)
$SW_LAT_D4 $SW_LON_D4
Master/inline grids - horiz. dimension: nx,ny
$NX_D4 $NY_D4
Master/inline grids - horiz. resol.: dx,dy (deg)
$res_D4 $res_D4
Nested master grids - vertical dimension: nz
 16
COORDINATE SYSTEM
spherical
ALL GRID VERTICAL COORDINATE SYSTEM
CCAM-SIGMA
Grid system: Arakawa grid class: grid_sys
A
ALL GRID CELL HEIGHT: in Hpascal temporaraily put up high as CCAM (.9974  to .9954)
0.997039,0.991134,0.982316,0.970636,0.959042,0.947535,0.930436,0.907940
0.885787,0.863977,0.831901,0.780135,0.682876,0.593808,0.512701,0.439293
 EACH GRID VECTOR LENGTHS FOR X-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Y-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR Z-advection
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 64
EACH GRID VECTOR LENGTHS FOR CHEMISTRY
NV SHOULD BE ABOUT 64 (WIN32) OR 1024 (NECSX)
 16
EACH GRID TIMESTEP FOR WHICH MET IS CONSTANT
 300
EACH GRID TIMESTEP FOR OPERATOR SPLITTING
 150
EACH GRID TIMESTEP output/SPStest: wrt_step(ng) (s)
 3600
STARTOPTIONS
DUMPCTM_WIND
48,48
DUMPCTM_TA
48,48
DUMPCTM_TV
48,48
DUMPCTM_THV
48,48
DUMPCTM_P
48,48
DUMPCTM_RHO
48,48
DUMPDIFF
48,48
WHE_2
-6.5
ANTHROPOGENIC
MOTOR_VEHICLE
COM_DOM
noINDUSTRIAL
$TRANSPORT
ADVECTION
HDIFFUSION
VDIFFUSION
LOCKSTEP
$DIFF
CHEMISTRY
NONITRATE
NOMARS
$SIOA
$SOA
NOCLOUDCHEMISTRY
DRYDEPOSITION
WETDEPOSITION
BIOGENIC
TREES
GRASS
noNATURALMERCURY
noNATURALWRITEHOURLY
USERBIOGENIC
nsw_biogenic_def.txt
BIOGENICLAI
ccam_${PNMI}_laiW_${MONTH}  ccam_${PNMI}_laiG_${MONTH}
noMERCURYSOIL
noausHg025.txt
SALT
SURF
20.
NOFIRE
AREASOURCEZ
3,3
whe,1,3,0.00,0.50,0.50
gse,1,3,0.33,0.33,0.33
vpx,1,3,0.64,0.27,0.09
NOSTACKTIPDOWNWASH
PLUMESMEAR
NOBRIGGSPLUMERISE
SCALE_MOTOR_VEHICLES (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_COMMERCIAL_DOMESTIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_INDUSTRIAL (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_FIRE (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_DUST (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
SCALE_NATURAL_BIOGENIC (NOX ROC PM10 CO)
1.0 1.0 1.0 1.0
NOSEABREEZECAP
NOVDIFFCIT
NOPBLCIT
NOSURFACEMET
NOWARM_START
NEST
NOWRITE_WARM_START
NOMET_INTERPOLATE
NOVECTOR
AODGC
4
ASO4,AS10,NIT,NH4
1.0,1.0,1.0,1.0
2
EC25,EC10
1.0,1.0
10
APA1,APA2,APA3,APA4,APA5,APA6,APA7,APA8,APA9,OC10
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
15
AOA1,AOA2,AOA3,AOA4,AOA5,AOA6,AOA7,AOA8,BOA1,BOA2,BOA3,BOA4,BOA5,BOA6,OOA
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
1
SS25
0.0
2
SS25 SS10
1.0 1.0
2
DU02,OT25
1.0,1.0
6
DU05,DU10,DU20,DU40,DU60,OT10
1.0,1.0,1.0,1.0,1.0,1.0
ENDOPTIONS
Mixing depth limits: hmin_urban,hmin_rural,hmax
80. 30. 2000.
Logical flag for urban stability
T F
Model code (DARLAM/LAPS/TAPM): NWP_code
CCAM
Only For CCAM run-- use 2d surface info from CCAm output/SPStest(T) or use from pre-processed CDF
T
Invert vertical arrays (T/F)
F
Path of preprocessed template netcdf file names
./
soil psd data file
soil_psd
soil parameter data file
soil_param
drought factor- revised
1. 0.2
Grid points per ctm grid point
1
Soiltype and LAI files for each grid
ccam_${PNMI}_soil
ccam_${PNMI}_lai_${MONTH}
eof
##############################################
# Clean up emission files
##############################################
#/bin/rm *.bin
