###########################################
# Plotting temperal data in stacked plots
# for each region
###########################################
# ACTIVATE OPENAIR #
library(openair)
library(timeSeries)
library(reshape2)
library(lattice)
# SET TIME ZONE TO Sydney/Australia #


cmp = c("U:", "E:", "//lidcofp01/User/monkk/", "C:/Users/monkk/Documents/")  # OEH (1) or UoW (2) computer or (3) external)
cmpout = c("U:", "E:", "\\\\lidcofp01\\User\\monkk\\", "C:\\Users\\monkk\\Documents\\")  # OEH (1) or UoW (2) computer or (3) external)
c = 4 


project <- "Non-road_Diesel"
case <- "S1a"

params_info <-read.csv(paste0(cmp[c],"Projects/",project,"/Rscripts/paramsList_ctm_only.csv"), header=TRUE)
params <- params_info[,1]
labs <- params_info[,2]

stns_info <- read.csv(paste0(cmp[c],"Projects/",project,"/Rscripts/GMRTList_2013_OEH.csv"), header=TRUE) 
stnname <-stns_info[,1]
site <-stns_info[,2]
latt <- stns_info[,3]
long <- stns_info[,4]
stnno <-stns_info[,5]
pm25_recorded <- stns_info[,9]
so2_recorded <- stns_info[,10]
co_recorded <- stns_info[,11]


period_start_AU <- as.POSIXct(c("2013-01-01"),  tz = "Etc/GMT-10")
period_end_AU <- as.POSIXct(c("2013-12-31"), tz = "Etc/GMT-10")
period_start <- as.POSIXct(format(period_start_AU,  tz = "UTC"), tz = "UTC")
period_end <- as.POSIXct(format(period_end_AU,  tz = "UTC"), tz = "UTC")

tests <- c("indFug")#,"pse_other", "vpx")

  lx <- 2
  
  alldata <- import(file = paste0(cmp[c], "Projects/",project,"/Data/",case,"/Alldata_",case,"_fullyear.csv"),
                    sep = ",", header.at = 1,
                    date.format = "%Y-%m-%d %H:%M:%S",tzone ="Etc/GMT-10")
  
  alldata_wide <- import(file = paste0(cmp[c], "Projects/",project,"/Data/",case,"/Alldata_",case,"_wide_fullyear.csv"),
                    sep = ",", header.at = 1,
                    date.format = "%Y-%m-%d %H:%M:%S",tzone ="Etc/GMT-10")
  
  #alldata$date <- as.POSIXct(format(alldata$date, tz = "Australia/Sydney"))
  #alldata$hour <- data.frame(time = format(alldata$date, "%H:%M", tz = "Australia/Sydney"))
#for (tt in 1:length(tests)){
    

  stns_4plot_wide <- subset(alldata_wide, stns == "Camden"| stns == "Chullora" | stns == "Richmond"| 
                         stns == "Kembla_Grange"| stns == "Newcastle" | stns == "Singleton" )
  stns_4plot <- subset(alldata, stns == "Camden"| stns == "Chullora" | stns == "Richmond"| 
                         stns == "Kembla_Grange"| stns == "Newcastle" | stns == "Singleton" )
  
 # alldata_stn <- subset(alldata, stns == as.character(stnname[i]) & date >= period_start[xp] & date <= period_end[xp])
#  alldata_stn <- subset(alldata, stns == as.character(stnname[i])) #& case == tests[tt] )
#  alldata_stn_wide <- subset(alldata_wide, stns == as.character(stnname[i]) & case == tests[tt])
#  alldata_stn_wide_noobs <- subset(alldata_wide, stns == as.character(stnname[i]) & case == tests[tt])
  
 #stns_4plot_wide_case <- subset(stns_4plot_wide, case == tests[tt])
 #stns_4plot_case <- subset(stns_4plot, case == tests[tt])
 
  firstparam = TRUE
  for ( p in 1:length(params) ) {
#p = 2
    
  
  #print(params[p])
    
  #if ( params[p] == "wd"){ 
  # pt <- "p"
  # pch_exp <-c(8,16,4)
  #  lty_exp <- NULL
  #} else {
    pt <- "l"
    pch_exp <- NULL
    lty_exp <- c(2,1,1)
   # }
  
  jpeg(paste0(cmpout[c],"Projects\\", project,"\\Plots\\TimeSeries\\",case,"\\",params[p],"\\TimePlot_",params[p],"_panel.jpeg"),
       width=10*300,height=15*300,res=300)
  timePlot(stns_4plot_wide, 
           pollutant =  names(stns_4plot_wide)[grep(pattern=paste0(params[p],"_"), names(stns_4plot_wide))],
           type = "stns", 
           #group = TRUE,
           cols= c( "Red","Blue"),#,"Black"),
           lty=c(1,2),#,1),
           lwd = 2,
           #pch = pch_exp,
           plot.type = pt,
           #key.columns = 2,
           layout = c(1,6),
           xlab="Date",
           ylab = paste0("Average ",labs[p]), group=TRUE)
  dev.off()
  
  jpeg(paste0(cmpout[c],"Projects\\", project,"\\Plots\\TimeSeries\\",case,"\\",params[p],"\\TimePlot_",params[p],"_panel_monthly.jpeg"),
       width=10*300,height=15*300,res=300)
  timePlot(stns_4plot_wide, 
           pollutant =  names(stns_4plot_wide)[grep(pattern=paste0(params[p],"_"), names(stns_4plot_wide))],
           type = "stns", 
           #group = TRUE,
           cols= c( "Red","Blue"),#,"Black"),
           avg.time = "month",
           lty=c(1,2),#,1),
           lwd = 2,
           #pch = pch_exp,
           plot.type = pt,
           #key.columns = 2,
           layout = c(1,6),
           xlab="Date",
           ylab = paste0("Average ",labs[p]), group=TRUE)
  dev.off()
  
 
}  # params loop
  
